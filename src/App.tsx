import React from "react";
import "./App.scss";
import "bootstrap/dist/css/bootstrap.min.css";
import Header from "./components/Header/Header";

function App() {
  const handle = () => {};
  return (
    <div className="App">
      <Header
        user={{}}
        onCheckout={handle}
        onSearchChange={handle}
        onLogin={handle}
        onLogout={handle}
        onCreateAccount={handle}
        itemsInCart={7}
      />
    </div>
  );
}

export default App;
