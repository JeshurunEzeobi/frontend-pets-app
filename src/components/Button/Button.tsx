import React, { DetailedHTMLProps, BaseHTMLAttributes } from "react";
import styles from "./button.module.scss";
import clsx from "clsx";

export interface ButtonProps
  extends DetailedHTMLProps<
    BaseHTMLAttributes<HTMLButtonElement>,
    HTMLButtonElement
  > {
  color?: "primary" | "secondary";
  backgroundColor?: string;
  size?: "small" | "medium" | "large";
  label: string;
  onClick?: () => void;
}

export const Button: React.FC<ButtonProps> = ({
  color = "primary",
  size = "medium",
  backgroundColor,
  label,
  ...props
}) => {
  const mode =
    color === "primary" ? styles.buttonPrimary : styles.buttonSecondary;
  return (
    <button
      type="button"
      className={clsx(
        styles.brandButton,
        size === "medium" && styles.buttonMedium,
        size === "small" && styles.buttonSmall,
        size === "large" && styles.buttonLarge,
        mode
      )}
      style={{ backgroundColor }}
      {...props}
    >
      {label}
    </button>
  );
};

export default Button;
