import React, { FC, ChangeEvent } from "react";
import HeaderContainer from "./components/HeaderContainer/HeaderContainer";

export interface HeaderProps {
  user?: {};
  itemsInCart?: number;
  onLogin: () => void;
  onLogout: () => void;
  onCreateAccount: () => void;
  onSearchChange: (event: ChangeEvent) => void;
  onCheckout: () => void;
}

const Header: FC<HeaderProps> = ({
  user,
  itemsInCart = 0,
  onLogin,
  onLogout,
  onCreateAccount,
  onSearchChange,
  onCheckout,
}) => {
  return (
    <HeaderContainer
      user={user}
      itemsInCart={itemsInCart}
      onLogin={onLogin}
      onLogout={onLogout}
      onCreateAccount={onCreateAccount}
      onSearchChange={onSearchChange}
      onCheckout={onCheckout}
    />
  );
};

export default Header;
