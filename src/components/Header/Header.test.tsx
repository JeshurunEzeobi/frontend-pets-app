import React, { ChangeEvent } from "react";
import Header from "./Header";
import { mount, shallow } from "enzyme";
import { render, fireEvent, waitFor } from "@testing-library/react";

describe("Header Component tests", () => {
  const call = jest.fn();
  const onSearchChange = jest.fn().mockImplementation((event: ChangeEvent) => {
    //
  });

  it("Renders without crashing", async () => {
    shallow(
      <Header
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={8}
      />
    );
  });

  it("Accepts Header props correctly", async () => {
    const wrapper = mount(
      <Header
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={8}
      />
    );
    expect(wrapper.props().itemsInCart).toEqual(8);
    expect(wrapper.props().onLogin).toEqual(call);
  });

  it("Should render about us", async () => {
    const { findByTestId } = render(
      <Header
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={7}
      />
    );

    const ContactUs = await findByTestId("header-about-us");
    expect(ContactUs).toBeInTheDocument();
  });

  it("It should call create accout function when account button is clicked", async () => {
    const { findByTestId } = render(
      <Header
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={2}
      />
    );

    const accountButton = await findByTestId("header-create-account");
    expect(accountButton).toBeInTheDocument();
    await waitFor(() => {
      fireEvent.click(accountButton);
      expect(call).toHaveBeenCalled();
    });
  });

  it("Should show free shipping text", async () => {
    const { getByText } = render(
      <Header
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={2}
      />
    );

    const text = getByText("Free shiping from 50 €");
    expect(text).toBeInTheDocument();
  });
});
