import React, { FC } from "react";
import AppBar from "@material-ui/core/AppBar";
import { makeStyles } from "@material-ui/core/styles";
import styles from "./HeaderContainer.module.scss";
import trustedIcon from "../../../../assets/images/eTrustedShops.webp";
import NavigationWidget from "../NavigationWidget/NavigationWidget";
import { HeaderProps } from "../../Header";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: "none",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
}));

const HeaderContainer: FC<HeaderProps> = ({
  user,
  itemsInCart = 0,
  onLogin,
  onLogout,
  onCreateAccount,
  onSearchChange,
  onCheckout,
}) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar position="sticky">
        <div className={styles.appbarUpperSection}>
          <div className={`w-75 text-center ${styles.freeShiping}`} data-testid="header-free-shipping-text">
            Free shiping from 50 €
          </div>
          <div className={"d-flex align-items-center"}>
            <img
              src={trustedIcon}
              alt="trusted shops"
              className={styles.eTrustShopIcon}
            />
            FAQ & Help
          </div>
        </div>
        <NavigationWidget
          itemsInCart={itemsInCart}
          onSearchChange={onSearchChange}
          onCreateAccount={onCreateAccount}
          onCheckout={onCheckout}
          onLogin={onLogin}
          onLogout={onLogout}
        />
      </AppBar>
    </div>
  );
};

export default HeaderContainer;
