import React from 'react';
// also exported from '@storybook/react' if you can deal with breaking changes in 6.1
import { Story, Meta } from '@storybook/react/types-6-0';
import HeaderListWidget, { HeaderListWidgetProps } from './HeaderListWidget';
export default {
  title: 'HeaderListWidget',
  component: HeaderListWidget,
} as Meta;

const Template: Story<HeaderListWidgetProps> = (args) => <HeaderListWidget {...args} />;

export const Dog = Template.bind({});
Dog.args = {
    open: true,
    dog: true,
    cat: false,
};

export const Cat = Template.bind({});
Cat.args = {
    open: true,
    dog: false,
    cat: true,
};