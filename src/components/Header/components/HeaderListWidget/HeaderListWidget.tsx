import React, { FC, ReactNode } from "react";
import styles from "./HeaderListWidget.module.scss";
import clsx from "clsx";

export interface HeaderListWidgetProps {
  cat: boolean;
  dog: boolean;
  open: boolean;
}

export type dropdownItemsProps = {
  href: string;
  name: string;
};

const Dot: FC<ReactNode> = () => <div className={styles.dot}>&bull;</div>;

const Li: FC<ReactNode> = ({ children }) => (
  <li>
    <Dot />
    {children}
  </li>
);

const HeaderListWidget: FC<HeaderListWidgetProps> = ({
  open = false,
  dog = false,
  cat = false,
}) => {
  const subListAims: dropdownItemsProps[] = [
    { name: "Lose Weight", href: "#" },
    { name: "Strong immune system", href: "#" },
    { name: "Coat & skin health", href: "#" },
    { name: "Better digestion", href: "#" },
    { name: "Hypoallergenic/Sensitive", href: "#" },
    { name: "Grain Free", href: "#" },
    { name: "Monoprotein", href: "#" },
    { name: "Sugar free", href: "#" },
  ];

  const subListAge: dropdownItemsProps[] = [
    { name: "Junior", href: "#" },
    { name: "Adult", href: "#" },
    { name: "Senior", href: "#" },
  ];

  const subListDeals: dropdownItemsProps[] = [
    { name: "Advantage sizes", href: "#" },
    { name: "Value packages", href: "#" },
  ];

  const createListItems = (list: dropdownItemsProps[]): ReactNode => {
    const ListItems = list.map((item, index) => (
      <a href={item.href} key={index}>
        {item.name}
      </a>
    ));
    return <div className={styles.dropdownContent}>{ListItems}</div>;
  };

  return (
    <div>
      {open && (
        <div className={clsx(styles.listContainer)} data-testid="HeaderListWidget-container">
          <ul className={clsx("list-inline", "d-center", "text-center")}>
            <Li>Fit paws</Li>
            <Li>Wet food</Li>
            <Li>dried animal food</Li>
            <Li>BARF</Li>
            {dog ? <Li>Green</Li> : null}
            <Li>Snacks</Li>
            <Li>Oils</Li>
            <Li>Nutritional supplement</Li>
            {dog ? <Li>health</Li> : null}
            {dog ? <Li>maintenance</Li> : null}
            <Li>equipment</Li>
          </ul>
          <div className="separator" />
          {cat ? (
            <div className={styles.dropdown}>
              <span className={styles.dropbtn}>
                <Dot /> aims
              </span>
              {createListItems(subListAims)}
            </div>
          ) : null}
          <div className={styles.dropdown}>
            <span className={styles.dropbtn}>
              <Dot /> Age
            </span>
            {createListItems(subListAge)}
          </div>
          <div className={styles.dropdown}>
            <span className={styles.dropbtn}>
              <Dot /> deals
            </span>
            {createListItems(subListDeals)}
          </div>
          <div className="separator" />
          <ul className={clsx("list-inline", "d-center")}>
            <Li>counselor</Li>
          </ul>
        </div>
      )}
    </div>
  );
};

export default HeaderListWidget;
