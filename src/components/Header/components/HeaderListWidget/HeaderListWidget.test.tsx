import React from "react";
import HeaderListWidget from "./HeaderListWidget";
import { mount, shallow } from "enzyme";
import { render, fireEvent, waitFor } from "@testing-library/react";

describe("HeaderListWidget Component tests", () => {
  it("Accepts props correctly", async () => {
    const wrapper = mount(
      <HeaderListWidget open={true} dog={true} cat={false} />
    );

    expect(wrapper.props().open).toEqual(true);
  });

  it("Not to be in Document", async () => {
    const { queryAllByTestId } = render(
      <HeaderListWidget open={false} dog={true} cat={false} />
    );
    const widget = queryAllByTestId("HeaderListWidget-container");
    expect(widget.length).toEqual(0);
  });

  it("aims dropdown when should be in document when cat button is clicked", async () => {
    const { findByText } = render(
      <HeaderListWidget open={true} dog={false} cat={true} />
    );

    const aimsDropdown = await findByText("aims");
    expect((await aimsDropdown).textContent).toBe("• aims");
  });

  it("Should open dropdown when deals is clicked", async () => {
    const { findByText } = render(
      <HeaderListWidget open={true} dog={true} cat={false} />
    );

    const dealsDropdown = await findByText("deals");
    await waitFor(async () => {
      fireEvent.click(dealsDropdown);
      const dropdownText = await findByText("Value packages");
      expect(dropdownText).toBeInTheDocument();
    });
  });
});
