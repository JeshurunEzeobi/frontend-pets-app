import React, { FC, Fragment, useState } from "react";
import InputBase from "@material-ui/core/InputBase";
import { fade, makeStyles } from "@material-ui/core/styles";
import SearchIcon from "@material-ui/icons/Search";
import Avatar from "@material-ui/core/Avatar";
import MallIcon from "@material-ui/icons/LocalMall";
import PersonIcon from "@material-ui/icons/Person";
import styles from "./NavigationWidget.module.scss";
import petsDeliLogo from "../../../../assets/svg/petsdeli-logo.svg";
import petsDeliLogoMobile from "../../../../assets/svg/petsdeli-logo-mobile.svg";
import clsx from "clsx";
import HeaderListWidget from "../HeaderListWidget/HeaderListWidget";
import { HeaderProps } from "../../Header";

const useStyles = makeStyles((theme) => ({
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    marginRight: theme.spacing(2),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(1),
      width: "auto",
    },
    [theme.breakpoints.down(1024)]: {
      display: "none",
    },
  },
  inputRoot: {
    color: "#19335a",
  },
  inputInput: {
    border: "1px solid #a6a6a6",
    borderRadius: "3rem",
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: theme.spacing(1),
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      width: "25ch",
      "&:focus": {
        width: "28ch",
      },
    },
  },
  inputRootMobile: {
    borderRadius: "0.25rem",
    color: "#19335a",
    width: "100%",
    marginLeft: "auto",
    marginRight: "auto",
    backgroundColor: "rgba(235,235,235,1)",
  },
  inputInputMobile: {
    borderRadius: "0.25rem",
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: theme.spacing(1),
    width: "100",
  },
  mallIcon: {
    position: "relative",
  },
  orange: {
    color: "#fff",
    backgroundColor: "rgba(246,149,100, 1)",
    position: "absolute",
    top: 2,
    right: -4,
    height: theme.spacing(2),
    width: theme.spacing(2),
    fontSize: theme.spacing(1.2),
    fontWeight: theme.spacing(0.8),
  },
}));

export type ButtonStateProps = {
  dog: boolean;
  cat: boolean;
};

const NavigationWidget: FC<HeaderProps> = ({
  user,
  itemsInCart = 0,
  onSearchChange,
  onCheckout,
  onCreateAccount,
  onLogin,
  onLogout,
}) => {
  const classes = useStyles();

  const buttonState = {
    dog: false,
    cat: false,
  };
  const [buttons, setButtons] = useState<ButtonStateProps>(buttonState);
  const [showMenu, setShowMenu] = useState<boolean>(false);

  const resetButtons = () => {
    setButtons(buttonState);
  };

  const handleClick = (data: Partial<ButtonStateProps>) => {
    resetButtons();
    setShowMenu(true);
    setButtons({ ...buttonState, ...data });
  };

  return (
    <Fragment>
      <div className="w-100 text-brand-primary" data-testid="NavigationWidget-container">
        <div className={styles.navigationButtons}>
          <img
            src={petsDeliLogo}
            alt="pets deli logo"
            className={styles.petsDeliLogo}
          />
          <img
            src={petsDeliLogoMobile}
            alt="pets deli logo"
            className={styles.petsDeliLogoMobile}
          />

          <div className="d-flex">
            <div
              className={clsx(buttons.dog && "theme-primary", styles.btn)}
              onClick={() => {
                handleClick({ dog: true });
              }}
            >
              FOR DOGS
            </div>
            <div
              className={clsx(
                buttons.cat && "theme-primary",
                "border-l",
                "border-r",
                styles.btn
              )}
              onClick={() => {
                handleClick({ cat: true });
              }}
            >
              FOR CATS
            </div>
            <div className={clsx("border-r", styles.btn)} onClick={() => {}}>
              CONSULTATION
            </div>
            <div className={clsx(styles.btn)} onClick={() => {}} data-testid="header-about-us">
              ABOUT US
            </div>
          </div>

          <div className="d-flex">
            <div className={classes.search}>
              <InputBase
                onChange={(event) => {
                  onSearchChange(event);
                }}
                placeholder="Futter finden"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                inputProps={{ "aria-label": "search" }}
              />
            </div>
            <div className={styles.iconsContainer} onClick={onCheckout}>
              <div className={classes.mallIcon}>
                <MallIcon color="inherit" />
                <Avatar className={classes.orange}>{itemsInCart}</Avatar>
              </div>
              <span>Shopping Cart</span>
            </div>
            <div className={styles.iconsContainer} onClick={onCreateAccount} data-testid="header-create-account">
              <PersonIcon color="inherit" />
              <span>Account</span>
            </div>
          </div>
          <div className={styles.searchMobileContainer}>
            <div className={styles.searchMobile}>
              <div className="w-80 relative">
                <InputBase
                  data-testid="header-search"
                  onChange={(event) => {
                    onSearchChange(event);
                  }}
                  placeholder="Futter finden"
                  classes={{
                    root: classes.inputRootMobile,
                    input: classes.inputInputMobile,
                  }}
                  inputProps={{ "aria-label": "search" }}
                />
                <SearchIcon color="inherit" />
              </div>
            </div>
          </div>
        </div>
        <HeaderListWidget open={showMenu} dog={buttons.dog} cat={buttons.cat} />
      </div>
    </Fragment>
  );
};

export default NavigationWidget;
