import React from 'react';
import { Story, Meta } from '@storybook/react/types-6-0';
import { HeaderProps } from '../../Header';
import NavigationWidget from './NavigationWidget';
import * as HeaderStories from '../../Header.stories';
export default {
  title: 'NavigationWidget',
  component: NavigationWidget,
} as Meta;

const Template: Story<HeaderProps> = (args) => <NavigationWidget {...args} />;

export const Default = Template.bind({});
Default.args = {
  ...HeaderStories.default.args,
};