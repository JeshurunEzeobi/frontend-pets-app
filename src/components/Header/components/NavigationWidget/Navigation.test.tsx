import React, { ChangeEvent } from "react";
import NavigationWidget from "./NavigationWidget";
import { shallow, mount } from "enzyme";
import { render, fireEvent, waitFor } from "@testing-library/react";
import { InputBase } from "@material-ui/core";

describe("NavigationWidget Component tests", () => {
  const call = jest.fn();
  const onSearchChange = jest.fn().mockImplementation((event: ChangeEvent) => {
    //
  });

  it("Should render without crashing", async () => {
    shallow(
      <NavigationWidget
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={8}
      />
    );
  });

  it("Accepts props correctly", async () => {
    const wrapper = mount(
      <NavigationWidget
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={8}
      />
    );
    expect(wrapper.props().itemsInCart).toEqual(8);
    expect(wrapper.props().onLogin).toEqual(call);
  });

  it("Cart should render number passed to itemsInCart", async () => {
    const number = 7;
    const { findByText } = render(
      <NavigationWidget
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={number}
      />
    );

    const numberInCart = await findByText("7");

    expect(numberInCart).toBeInTheDocument();
  });

  it("It should call create accout function when account button is clicked", async () => {
    const { findByText } = render(
      <NavigationWidget
        user={{}}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={2}
      />
    );

    const dogsButton = await findByText("FOR DOGS");
    expect(dogsButton).toBeInTheDocument();
    await waitFor(async () => {
      fireEvent.click(dogsButton);
      const link = await findByText("Green");
      expect(link).toBeInTheDocument();
    });
  });

  it("should call onChange props", async () => {
    const event = {
      preventDefault() {},
      target: { value: "the-value" },
    };
    const component = shallow(
      <NavigationWidget
        user={{ name: "Jeshurun", email: "myemail.com" }}
        onCheckout={call}
        onCreateAccount={call}
        onLogin={call}
        onLogout={call}
        onSearchChange={onSearchChange}
        itemsInCart={18}
      />
    );

    await waitFor(() => {
      component.find(InputBase).at(0).simulate("change", event);
      expect(onSearchChange).toBeCalled();
    });
  });
});
